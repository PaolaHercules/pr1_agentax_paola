<?php 

namespace App\Entities;
use CodeIgniter\Entity;

class Taxi extends Entity{

	private $id_taxi;
	private $descripcion;
	private $id_estado;

	public function getIdTaxi(){
		return $this->id_taxi;
	}

	public function setIdTaxi($id_taxi){
		$this->id_taxi = $id_taxi;
	}

	public function getDescripcion(){
		return $this->descripcion;
	}

	public function setDescripcion($descripcion){
		$this->descripcion = $descripcion;
	}

	public function getEstado(){
		return $this->id_estado;
	}

	public function setIdEstado($id_estado){
		$this->id_estado = $id_estado;


	}
}