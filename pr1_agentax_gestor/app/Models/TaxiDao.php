<?php 

namespace App\Models;

use App\Entities\Taxi;
use CodeIgniter\Model;

class TaxiDao extends Model{

	protected $table = 'taxi';

	public function getTaxi(){
		$sp_consultar_taxi = 'CALL sp_consultar_taxi()';
		$query = $this->query($sp_consultar_taxi);

		$Taxi = array();

		foreach($query->getResult() as $r){
			$vx = new Taxi();
			$vx->setIdTaxi($r->id_taxi);
			$vx->setDescripcion($r->descripcion);
			$vx->setIdEstado($r->estado);
			array_push($Taxi,$vx);
		}

		return $Taxi;
	}

	public function getEstado(){
		$getEstado = 'CALL getEstado()';
		$query = $this->query($getEstado);

		return $query->getResult();
	}

	public function ingresar($new){
		$sp_ingresar_taxi = 'CALL sp_ingresar_taxi(?,?)';
		$query = $this->query($sp_ingresar_taxi, [$new->getDescripcion(), $new->getEstado()]);

		if($query){
			return "add";
		}else{
			return "errorA";
		}
	}

	public function eliminar($id){
		$sp_eliminar_taxi = 'CALL sp_eliminar_taxi()';
		$query = $this->query($sp_eliminar_taxi,[$id]);

		if ($query) {
			return true;
		}else{
			return false;
		}
	}
}