<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$views= view('template/header').view('template/navbar').view('welcome_message').view('template/footer');
		return $views;
	}

	//--------------------------------------------------------------------

}
