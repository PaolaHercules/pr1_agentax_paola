<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Entities\Taxi;
use App\Models\TaxiDao;

class AccionesGestor extends Controller{

	public function index(){
		$taxi = new TaxiDao();

		$data['taxi'] = $taxi->getTaxi();
		$data['estado'] = $taxi->getEstado();

		echo view('template/header', $data);
		echo view('template/navbar');
		echo view('Taxis');
		echo view('template/footer');
	}

	public function ingresar(){
		$taxi = new TaxiDao();

		$new  = new Taxi();
		$new->setDescripcion($this->request->getPost('descripcion'));
		$new->setIdEstado($this->request->getPost('estado'));

		$msj = $taxi->ingresar($new);

		if($msj == "add"){
			echo "<script>alert('Agregado exitosamente')</script>";
		}else{
			echo "<script>alert('Error al agregar')</script>";
		}

		return redirect()->to('/pr1_agentax_gestor/public/taxi');
	}

	public function eliminar($id){
		$taxi = new TaxiDao();
		$msj = $taxi->eliminar($id);

		if($msj == true){
			echo "<script>alert('Eliminado con exito')</script>";
		}else{
			echo "<script>alert('Error al Eliminar')</script>";
		}

		return redirect()->to('/pr1_agentax_gestor/public/taxi');
	}
}