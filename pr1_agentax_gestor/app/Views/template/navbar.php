<body class="container">

	<!--Navbar cliente-->
<!-- 	<nav class="navbar navbar-light bg-light">
		<a class="navbar-brand" href="#">AGENTAX</a>
    <div style="float: right; font-size: 1.25em">
		<a style="color: gray"  href="#" data-toggle="modal" data-target="#modalClienteConfig"><i class="fas fa-cog"></i></a>
    <a style="color: gray"  href="#" data-toggle="modal" data-target="#Exit"><i class="fas fa-sign-out-alt"></i></a>
    </div>
  </nav> -->
  <!--Fin Navbar cliente-->

  <!--Modal cliente configuraciones-->
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalClienteConfig">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Modal body text goes here.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- FinModal cliente configuraciones-->


<!--Navbar TAXISTA
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">AGENTAX</a>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#">Asignaciones</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Historial</a>
      </li>
    </ul>
  </div>
   <a style="float: right; color: gray; font-size: 1.25em"  href="#" data-toggle="modal" data-target="#Exit"><i class="fas fa-sign-out-alt"></i></a>
 </nav>-->
 <!--Fin Navbar TAXISTA-->



 <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">AGENTAX</a>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
       <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Solicitudes
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="#">Action</a>
        <a class="dropdown-item" href="#">Another action</a>
        <a class="dropdown-item" href="#">Another action</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">Reportes</a>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?= base_url('Controllers/GestorController') ?>">Taxis</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Taxistas</a>
    </li>
  </ul>
</div>
<a style="float: right; color: gray; font-size: 1.25em"  href="#" data-toggle="modal" data-target="#Exit"><i class="fas fa-sign-out-alt"></i></a>
</nav>
<!--Fin Navbar Gestor-->



<!--Modal exit  Modal de confirmación para cerrar sesión para cualquier navbar-->
<div class="modal" tabindex="-1" role="dialog" id="Exit">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>¿Desea cerrar sesión?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Sí, salir</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<!--Fin Modal exit-->
