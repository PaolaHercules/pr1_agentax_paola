<body>
	<br>
	<p>
		<button class="btn btn-info" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
			Ingresar taxi
		</button>
	</p>
	<div class="collapse" id="collapseExample">
		<div class="card card-body">
			<form action="<?php echo base_url('/pr1_agentax_gestor/public/taxi/ingresar') ?>" method="post">
				<table>
					<tr>
						<td><label>Descripción</label></td>
						<td><input type="text" name="descripcion" class="form-control"></td>
					</tr>
					<tr>
						<td><label for="estado">Estado</label></td>
						<td><select name="estado" id="estado" class="form-control">
							<option value="">--Seleccione un estado--</option>
							<?php foreach($estado as $e)  { ?>
								<option value="<?php echo $e->id_estado ?>"><?php echo $e->estado ?></option>
							<?php  } ?>
						</select></td>
					</tr>
				</table>
				<input type="submit" name="Enviar" class="btn btn-primary">
			</form>
		</div>
	</div><br>


	<table class="table table-hover table-bordered">
		<thead class="thead-dark">
			<tr>
				<th scope="col">#</th>
				<th scope="col">Descripcion</th>
				<th scope="col">Estado</th>
				<th colspan="3">Operaciones</th>
			</tr>
		</thead>
		<tbody>
			<?php $n=1; foreach($taxi as $t) {?>
				<tr>
					<td><?= $n; ?></td>
					<td><?= $t->descripcion; ?></td>
					<td><?= $t->estado; ?></td>
					<td><a class="btn btn-danger" onclick="return confirm('Desea realmente eliminar este dato?')" href="/pr1_agentax_gestor/public/AccionesGestor/eliminar/<?php echo $t->id_taxi;  ?>">Eliminar</a></td>
					<td><a class="btn btn-info" >Modificar</a></td>
					<td><a class="btn btn-warning" >Dar de baja</a></td>
				</tr>
				<?php $n++;} ?>
			</tbody>
		</table>


	</body>